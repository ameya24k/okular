#ifndef _NCSAFONTLETTERINFO_H_
#define _NCSAFONTLETTERINFO_H_


#include <qwidget.h>
#include <QLabel>
#include <QComboBox>
#include <QDialog>
#include <qlayout.h>


#include "ncsawordspottingutil.h"
#include "ncsafonteditor.h"
#include "ncsapreprocessdialog.h"
#include "ncsaimagelabel.h"
#include "ncsabuildfontdialog.h"


#include <QListWidget>
#include <QListWidgetItem>
#include <QStackedWidget>
#include <QScrollArea>
#include <QButtonGroup>
#include <map>
#include <vector>
#include <QLabel>
#include <QRadioButton>
#include <QCheckBox>


class FontLetterInfo
{
public:
  QPixmap img;
  int width;
  QPoint topLeft;
};

#endif
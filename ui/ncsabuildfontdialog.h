#ifndef _NCSABUILDFONTDIALOG_H_
#define _NCSABUILDFONTDIALOG_H_

#include <qwidget.h>
#include <QLabel>
#include <QComboBox>
#include <QDialog>
#include <qlayout.h>

#include <core/document.h>
#include <core/observer.h>

#include <leptonica/allheaders.h>

#include "ncsafindbar.h"
#include "ncsawordspottingutil.h"
#include "ncsafonteditor.h"
#include "ncsapreprocessdialog.h"
#include "ncsaimagelabel.h"
#include "ncsafontletterinfo.h"
#include <poppler/qt4/poppler-qt4.h>

#include <QListWidget>
#include <QListWidgetItem>
#include <QStackedWidget>
#include <QScrollArea>
#include <QButtonGroup>
#include <map>
#include <vector>
#include <QLabel>
#include <QRadioButton>
#include <QCheckBox>

class NCSAFindBar;
class NCSAFontEditor;
class FontLetterInfo;

class BuildFontDialog:public QDialog
{
  Q_OBJECT

public:
    BuildFontDialog(NCSAFindBar *bar, Poppler::Document *pdf);
    
    static QPixmap renderFont2Pix(std::map<char, FontLetterInfo> builtFont, QString str);
    

public slots:
    void goPrevious();
    void goNext();
    void testFun();
    void displayOriginalPage();
    void displayBoxedPage();
    void pickLetter(const QPoint & p);
    void saveToFile();
    void testInputChanged(QString);
    void displayFontEditorFor(QAbstractButton*);


private:

    NCSAFindBar *ncsaFindBar;
    QListWidget *contentsWidget;
    QStackedWidget *pagesWidget;
    QScrollArea * scrollArea;
    ImageLabel * imageLabel;
    QLabel * pageIndicator;
    QImage originalPage;
    QImage boxedPage;
    
    QLineEdit *testInput;
    QLabel * testDisplay;

    Poppler::Document *doc;
    int currentPage;
    
    void displayPage();
    void createLetters(QHBoxLayout *lowerCases, QHBoxLayout *upperCases);
    QButtonGroup *buttonGroup;
    QButtonGroup isBoxedDisplay;
    std::map<QAbstractButton*, QLabel*> radio2label;
    std::vector<QRect> letterRects;
    std::vector<int> baselines;
    std::map<QRect, QRect> tightLetterBox2Box;
    

    std::map<QRadioButton *, char> radioBtn2Char;
    QCheckBox *selectionArbitrary;
    QPoint arbitraryLastPoint;
    NCSAFontEditor *fontEditor;
    
    std::map<char, FontLetterInfo> builtFont;

};



#endif

/***************************************************************************
 *   Copyright (C) 2013 by Haidong Tang                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef _NCSAPARAMETERS_H_
#define _NCSAPARAMETERS_H_

//static int NCSA_RESOLUTION = 250;
//static int NCSA_BLOCKSIZE = 3;
//static int NCSA_C = 10;
//static int NCSA_OVERLAP = 0.6;
class NCSAParameters
{
private:
static int resolution;

public:
static void initialization();
};



#endif
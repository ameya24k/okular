/***************************************************************************
 *   Copyright (C) 2007 by Pino Toscano <pino@kde.org>                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include "ncsafindbar.h"

// qt/kde includes
#include <qlabel.h>
#include <qmenu.h>
#include <qtoolbutton.h>
#include <qevent.h>
#include <kicon.h>
#include <klocale.h>
#include <kpushbutton.h>

// local includes
#include "searchlineedit.h"
#include "core/document.h"
#include <core/page.h>
#include "settings.h"

#include <QFontDatabase>
#include <qcombobox.h>
#include <QStringList>
#include <QPainter>
#include <QFileDialog>

#include <core/generator.h>
#include <tesseract/baseapi.h>
#include <tesseract/resultiterator.h>
#include <opencv2/opencv.hpp>

#include <kmessagebox.h>

#include "ncsaparameters.h"

NCSAFindBar::NCSAFindBar( Okular::Document * document, QWidget * parent )
  : QWidget( parent )
  , m_active( false )
{
    doc = document;
    if(NULL != doc)
    {
      doc->addObserver(this);
    }
    QVBoxLayout * vlay = new QVBoxLayout( this );
    QHBoxLayout * displayRow = new QHBoxLayout();
    vlay->addLayout( displayRow );
    display = new QLabel("");
    displayRow->addWidget(display);
    displayRow->insertStretch(1);
    resultComboBox = new QComboBox();
    //resultComboBox->addItems(fonts);
    displayRow->addWidget(resultComboBox);

    QHBoxLayout * lay = new QHBoxLayout();
    vlay->setMargin( 2 );
    vlay->addLayout(lay);


    QPushButton * optionsBtn = new QPushButton( this );
    optionsBtn->setText( i18n( "NCSA Options" ) );
    optionsBtn->setToolTip( i18n( "Modify search behavior" ) );
    QMenu * optionsMenu = new QMenu( optionsBtn );
    m_buildFontAct = optionsMenu->addAction( i18n( "Create custom font" ) );
    m_useSystemFontAct = optionsMenu->addAction( i18n( "use system font" ) );
    m_useCustomFontAct = optionsMenu->addAction( i18n( "use custom font" ) );
    m_handwrittenSegCB = optionsMenu->addAction( i18n( "use handwritten segmentation" ) );
    m_useSystemFontAct->setCheckable(true);
    m_useCustomFontAct->setCheckable(true);
    m_useSystemFontAct->setChecked(true);
    m_useCustomFontAct->setChecked(false);
    m_handwrittenSegCB->setCheckable(true);
    m_handwrittenSegCB->setChecked(false);
    connect( m_buildFontAct, SIGNAL(triggered(bool)), this, SLOT(buildFontAct(bool)) );
    connect( m_useSystemFontAct, SIGNAL(triggered(bool)), this, SLOT(fontSourceAsSystem(bool)) );
    connect( m_useCustomFontAct, SIGNAL(triggered(bool)), this, SLOT(fontSourceAsCustom(bool)) );
    
    optionsBtn->setMenu( optionsMenu );
    lay->addWidget( optionsBtn );
    
    QStringList fonts = QFontDatabase().families();
    
    fontComboBox = new QComboBox();
    fontComboBox->addItems(fonts);
    lay->addWidget(fontComboBox);
    connect( fontComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeFont(int)) );
    connect( resultComboBox, SIGNAL(highlighted(int)), this, SLOT(resultComboBoxIndexChanged(int)) );
    connect( resultComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(resultComboBoxIndexSelected(int)) );
    
    
    loadFontBtn = new QPushButton( KIcon( "Load font" ), i18nc( "Load font", "load" ), this );
    lay->addWidget( loadFontBtn );
    loadFontBtn->setVisible(false);
    connect( loadFontBtn, SIGNAL(clicked()), this, SLOT(loadFont()) );

    QPalette p(palette());
    p.setColor(QPalette::Background, Qt::white);
    display->setAutoFillBackground(true);
    display->setPalette(p);
    display->setText("You input will be displayed here");
    QFont font;
    font.setPointSize(32);
    font.setBold(true);
    font.setFamily(fonts.at(16));

    display->setFont(font);
    
    searchLine = new QLineEdit();
    lay->addWidget(searchLine);
    
    connect(searchLine,SIGNAL(textChanged(QString)),this,SLOT(searchLineTextChanged(QString)));
    hide();

    
    QPushButton * preprocessBtn = new QPushButton( KIcon( "Preprocess" ), i18nc( "Search using word spotting technique", "preprocess" ), this );
    preprocessBtn->setToolTip( i18n( "Search using word spotting technique" ) );
    lay->addWidget( preprocessBtn );
    connect( preprocessBtn, SIGNAL(clicked()), this, SLOT(bringPreprocessDialog()) );
    
    
    QPushButton * searchBtn = new QPushButton( KIcon( "Search" ), i18nc( "Search using word spotting technique", "search" ), this );
    searchBtn->setToolTip( i18n( "Search using word spotting technique" ) );
    lay->addWidget( searchBtn );
    connect( searchBtn, SIGNAL(clicked()), this, SLOT(performSearch()) );
    
    // "activate" it only at th very end
    m_active = true;
    wordSpottingUtil = NULL;
    
    NCSA_RESOLUTION = 250;
    NCSA_BLOCKSIZE = 3;
    NCSA_C = 10;
    NCSA_OVERLAP = 0.6;
    lastProcessedResolution = 250;
    lastProcessedBlockSize = 3;
    lastProcessedC = 10;
    lastProcessedOverlap = 0.6;
    lastProcessed = false;
    
}

NCSAFindBar::~NCSAFindBar()
{
}

bool NCSAFindBar::isHandwrittenMode() {
	return m_handwrittenSegCB->isChecked();
}

void NCSAFindBar::loadFont() {
	QString path = QFileDialog::getExistingDirectory(this, "Open");
	QFile file(path + "/fontInfo.txt");
   if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {}

   QTextStream in(&file);
   loadedFont.clear();
   while (!in.atEnd())
   {
      QString line = in.readLine();
      QStringList lstLine = line.split(",");
      char letter = lstLine.at(0).at(0).toAscii();
      int width = lstLine.at(1).toInt();
      int x = lstLine.at(2).toInt();
      int y = lstLine.at(3).toInt();
      QPixmap img;
      img.load(path + "/" + letter + ".png");
      
      FontLetterInfo info;
      info.img = img;
      info.topLeft = QPoint(x,y);
      info.width = width;

      loadedFont[letter] = info;
   }
   file.close();
	}
	
void NCSAFindBar::setPreprocessParameters(int res, int bs, int c, double ol) {

	NCSA_RESOLUTION = res;
   NCSA_BLOCKSIZE = bs;
   NCSA_C = c;
   NCSA_OVERLAP = ol;
   qDebug() << "setting: " << NCSA_RESOLUTION << " " << NCSA_BLOCKSIZE << " " << NCSA_C << " " << NCSA_OVERLAP <<endl;
}
void NCSAFindBar::fontSourceAsSystem(bool systemFont) {
		switchToSystemFont(true);
}
	
void NCSAFindBar::fontSourceAsCustom(bool customFont) {
		switchToSystemFont(false);
	}
	
void NCSAFindBar::switchToSystemFont(bool flag) {
		if (flag) {
			m_useSystemFontAct->setChecked (true);
			m_useCustomFontAct->setChecked (false);
			fontComboBox->setVisible(true);
			loadFontBtn->setVisible(false);

	} else {
		m_useSystemFontAct->setChecked (false);
			m_useCustomFontAct->setChecked (true);
			fontComboBox->setVisible(false);
			loadFontBtn->setVisible(true);
	}
	}
void NCSAFindBar::buildFontAct(bool b)
{
  QString filepath =  getFilePath();
  Poppler::Document *pdf = Poppler::Document::load(filepath);  
  BuildFontDialog* buildFontDialog = new BuildFontDialog(this, pdf);
  buildFontDialog->show();
}

QString NCSAFindBar::getFilePath()
{
   const QDomDocument * ds = (const QDomDocument *)(doc->documentInfo());
   if(ds == NULL)
   {
     QMessageBox::warning(0, QString("Error"), QString("Cannot get file path."));
     return "";
   }
   QString filename = ds->toString();
   int begin_idx = filename.indexOf("<filePath title=\"File Path\" value=") ;
   int end_idx = filename.indexOf("/>", begin_idx);
   return filename.mid(begin_idx+42, end_idx - begin_idx-43);
  
}

  
void NCSAFindBar::notifyPageChanged(int page, int flags)
{
  qDebug() << "request callback";
}

void NCSAFindBar::resultComboBoxIndexChanged(int index)
{
       NCSAWordInfo* wordInfo = searchResult[index];
       emit searchResultSelected(wordInfo->pagenum,
				     wordInfo->box->x * 1.0 / wordInfo->width, 
				     wordInfo->box->y * 1.0 / wordInfo->height, 
			             wordInfo->box->w * 1.0 / wordInfo->width, 
			             wordInfo->box->h * 1.0 / wordInfo->height);

}

void NCSAFindBar::resultComboBoxIndexSelected(int index) {
	if (index >= resultComboBox->count() || index < 0) {
		return;
	}
	NCSAWordInfo* wordInfo = searchResult[index];
	emit searchResultSelectedShouldScroll(wordInfo->pagenum, wordInfo->box->y * 1.0 / wordInfo->height);
}

void NCSAFindBar::preprocessDocument()
{

     if(wordSpottingUtil != NULL && !m_handwrittenSegCB->isChecked())
     {
         return;
     }
     
     if(wordSpottingUtil != NULL && m_handwrittenSegCB->isChecked() && lastProcessed 
     && lastProcessedResolution == NCSA_RESOLUTION
     && lastProcessedBlockSize == NCSA_BLOCKSIZE
     && lastProcessedC == NCSA_C
     && lastProcessedOverlap == NCSA_OVERLAP)
     {
         return;
     }
     
     if (m_handwrittenSegCB->isChecked()) {
         lastProcessedResolution = NCSA_RESOLUTION;
         lastProcessedBlockSize = NCSA_BLOCKSIZE;
         lastProcessedC = NCSA_C;
         lastProcessedOverlap = NCSA_OVERLAP;
         
     }
  
     if(wordSpottingUtil == NULL) {
       wordSpottingUtil = new NCSAWordSpottingUtil();
     }
     
     QString filepath =  getFilePath();
     Poppler::Document *pdf = Poppler::Document::load(filepath);  
     if(pdf == NULL)
     {
        QMessageBox::warning(0, QString("Error"), QString("Cannot locate file at path: ").append(filepath).append(" ."));
        return;
     }
     wordSpottingUtil->clearPreprocessResult();  
     lastProcessed = true;
     for(int i = 0; i < pdf->numPages(); i++)
     {
       
       if (m_handwrittenSegCB->isChecked()) {
         wordSpottingUtil->addPage(pdf->page(i)->renderToImage(NCSA_RESOLUTION, NCSA_RESOLUTION).convertToFormat(QImage::Format_RGB888), i, NCSA_BLOCKSIZE, NCSA_C, NCSA_OVERLAP);
       } else {
         wordSpottingUtil->addPage(pdf->page(i)->renderToImage(150, 150), i);
       }
       
     }
}

void NCSAFindBar::bringPreprocessDialog() {
	
	QString filepath =  getFilePath();
  Poppler::Document *pdf = Poppler::Document::load(filepath);  
  PreprocessDialog* preprocessDialog = new PreprocessDialog(this, pdf);
  preprocessDialog->show();
  
	
	}

void NCSAFindBar::performSearch()
{
           qDebug() << "ress:" << NCSA_RESOLUTION << "  blocksize:" << NCSA_BLOCKSIZE << "  c:" << NCSA_C << "  overlap:" << NCSA_OVERLAP;

    this->preprocessDocument();
    qDebug() << "b1" << endl;

  //if (!m_handwrittenSegCB->isChecked()) {
     
     QPixmap search_input(display->size());
     display->render(&search_input);     
     qDebug() << "b2" << endl;
     searchResult = wordSpottingUtil->search(search_input, 10);
     qDebug() << "b3" << endl;

     QString filepath =  getFilePath();
     qDebug() << "b3.1" << endl;
     Poppler::Document *pdf = Poppler::Document::load(filepath);  
     qDebug() << "b3.2" << endl;
     ////////displaying results
     //resultComboBox->clear();
     int numberOfItems = resultComboBox->count();

     for (int i = (numberOfItems-1); i >= 0 ; i--){
     	 qDebug() << "b3.22" << endl;
        resultComboBox->removeItem(i);
     }
     qDebug() << "b3.3" << endl;
     //QImage returnedPage = pdf->page(wordInfo->pagenum)->renderToImage(NCSA_RESOLUTION, NCSA_RESOLUTION);
     
     QImage returnedPage = pdf->page(0)->renderToImage(NCSA_RESOLUTION, NCSA_RESOLUTION);
     for(int i = 0; i < searchResult.size(); i++)
     {
     	 qDebug() << "b4" << endl;
       NCSAWordInfo* wordInfo = searchResult[i];
       qDebug() << "b5" << endl;
        //TODO: get rid of having to read the file again
       //QImage returnedPage = *(wordInfo->page);
       qDebug() << "b6" << endl;
       const QImage word = returnedPage.copy(wordInfo->box->x, wordInfo->box->y, wordInfo->box->w, wordInfo->box->h);
       qDebug() << "b7" << endl;
       resultComboBox->addItem(QPixmap::fromImage(word), "", -1);
       returnedPage.height();
       returnedPage.width();

     }
     qDebug() << "10" << endl;
     QSize size(100,30);
     resultComboBox->setIconSize(size);
     
     /*
  } else {
  	qDebug() << "use handwritten segmentation";
  	QString filepath =  getFilePath();
  	Poppler::Document *pdf = Poppler::Document::load(filepath);  
  	for(int i = 0; i < pdf->numPages(); i++)
   {
       QImage page = pdf->page(i)->renderToImage(200, 200).convertToFormat(QImage::Format_RGB888);
       cv::Mat output = NCSAWordSpottingUtil::convertToBinary(page, 3, 10);
       
       
       QImage preview = NCSAWordSpottingUtil::cvmat2QImage(output);
       
       vector<int> boundaries = NCSAWordSpottingUtil::findLineBoundaries(output);
       
       cv::Mat drawing = output.clone();
       for(int i = 1; i < boundaries.size(); i++)
       {
       	 cv::Mat line = output.rowRange(boundaries[i-1], boundaries[i]);
       	 cv::Mat drawing_line = drawing.rowRange(boundaries[i-1], boundaries[i]);
       	 cv::dilate(line, line, cv::Mat(), cv::Point(-1,-1));
       	 
       	 vector<vector<cv::Point> > contours;
       	 vector<cv::Vec4i> hierarchy;
       	 
       	 cv::findContours(line, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
       	 
       	 for (int i = 0; i < contours.size(); i++) {
       	 	cv::Rect r = cv:: boundingRect(contours[i]);
       	 	if (r.br().x - r.tl().x < 10 || r.br().y - r.tl().y < 10) {
       	 		continue;
       	 	}
       	 	
       	 	cv::rectangle(drawing_line, r.tl(), r.br(), cv::Scalar(255), 1, 8, 0);
       	 	//qDebug() << "rect: " << r.tl().x << " " << r.tl().y << " " << r.br().x << " " << r.br().y << " ";
       	 }
       	 
       	 //qDebug() << "sdfsdf: " << contours.size();
       	 
          QString path;
          path.append("/home/htang14/intermediateresults/line");
          path.append(i);
          path.append(".png");
       	 cv::imwrite(path.toLocal8Bit().data(), drawing_line );
       }

       cv::imwrite( "/home/htang14/intermediateresults/interm3.png", drawing );
       page.save("/home/htang14/intermediateresults/preview.png");
       

   }
  	}
     
     
     */
}
void NCSAFindBar::searchLineTextChanged(QString text)
{
	 if (m_useSystemFontAct->isChecked()) {
		display->setText(text);
	 	} else {

	 		display->setPixmap(BuildFontDialog::renderFont2Pix(loadedFont, text));
	 		}

}

void NCSAFindBar::saveTempPics()
{
    
}

void NCSAFindBar::changeFont(int index)
{
    QFont newfont;
    newfont.setPointSize(32);
    newfont.setBold(true);
    newfont.setFamily(fontComboBox->itemText(index));
    display->setFont(newfont);
}

#include "ncsafindbar.moc"

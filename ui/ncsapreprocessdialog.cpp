#include "ncsapreprocessdialog.h"

#include <qlabel.h>
#include <qmenu.h>
#include <qtoolbutton.h>
#include <qevent.h>
#include <kicon.h>
#include <klocale.h>
#include <kpushbutton.h>

// local includes
#include "searchlineedit.h"
#include "core/document.h"
#include <core/page.h>
#include "settings.h"

#include <QFontDatabase>
#include <qcombobox.h>
#include <QStringList>
#include <QPainter>
#include <QFileDialog>


#include <core/generator.h>
#include <tesseract/baseapi.h>
#include <tesseract/resultiterator.h>
#include <opencv2/opencv.hpp>

#include <kmessagebox.h>

#include "ncsaparameters.h"


PreprocessDialog::PreprocessDialog(NCSAFindBar *bar, Poppler::Document *pdf)
{
	 ncsaFindBar = bar;

    doc = pdf;
    currentPage = 0;
    

    QPushButton *confirmButton = new QPushButton(tr("OK"));
    QPushButton *closeButton = new QPushButton(tr("Cancel"));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(confirmButton, SIGNAL(clicked()), this, SLOT(testFun()));

    imageLabel = new ImageLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);
    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);

    
    QPushButton *previousButton = new QPushButton(tr("Previous"));
    QPushButton *nextButton = new QPushButton(tr("Next"));
    connect(previousButton, SIGNAL(clicked()), this, SLOT(goPrevious()));
    connect(nextButton, SIGNAL(clicked()), this, SLOT(goNext()));
    pageIndicator = new QLabel;

    QHBoxLayout *pageNavigationLayout = new QHBoxLayout;
    pageNavigationLayout->addStretch();
    pageNavigationLayout->addWidget(previousButton);
    pageNavigationLayout->addWidget(pageIndicator);
    pageNavigationLayout->addWidget(nextButton);
    pageNavigationLayout->addStretch();



    resolutionSpinBox = new QSpinBox;
    resolutionSpinBox->setRange(50, 500);
    resolutionSpinBox->setSingleStep(10);
    resolutionSpinBox->setValue(ncsaFindBar->NCSA_RESOLUTION);
    
    blockSizeSpinBox = new QSpinBox;
    blockSizeSpinBox->setRange(3, 21);
    blockSizeSpinBox->setSingleStep(2);
    blockSizeSpinBox->setValue(ncsaFindBar->NCSA_BLOCKSIZE);

    CSpinBox = new QSpinBox;
    CSpinBox->setRange(-50, 50);
    CSpinBox->setSingleStep(1);
    CSpinBox->setValue(ncsaFindBar->NCSA_C);
    
    overlapSpinBox = new QDoubleSpinBox;
    overlapSpinBox->setRange(0,1);
    overlapSpinBox->setDecimals(1); 
    overlapSpinBox->setSingleStep(0.1);
    overlapSpinBox->setValue(ncsaFindBar->NCSA_OVERLAP); 
    
    QLabel *label1 = new QLabel("Resolution:");
    QLabel *label2 = new QLabel("Block size:");
    QLabel *label3 = new QLabel("C:");
    QLabel *label4 = new QLabel("Overlap");

    QHBoxLayout *parameterLayout = new QHBoxLayout;
    parameterLayout->addStretch();
    parameterLayout->addWidget(label1);
    parameterLayout->addWidget(resolutionSpinBox);
    parameterLayout->addStretch();
    parameterLayout->addWidget(label2);
    parameterLayout->addWidget(blockSizeSpinBox);
    parameterLayout->addStretch();
    parameterLayout->addWidget(label3);
    parameterLayout->addWidget(CSpinBox);
    parameterLayout->addStretch();
    parameterLayout->addWidget(label4);
    parameterLayout->addWidget(overlapSpinBox);
    parameterLayout->addStretch();
    
    QVBoxLayout *infoLayout = new QVBoxLayout;
    
    QLabel *resInfo = new QLabel("Resolution: The resolution at which the page will be rendered. Higher resolution will increase processing time.");
    QLabel *blockSizeInfo = new QLabel("Block size: Adjusting this parameter will affect the smoothness of the binary thresholded image.");
    QLabel *CInfo = new QLabel("C: The threshold used when thresholding a colored page into a binary page.");
    QLabel *OverlapInfo = new QLabel("Overlap: The overlap percentage threshold above which two adjacent words will be merged into a single word.");
    
    infoLayout->addWidget(resInfo);
    infoLayout->addWidget(blockSizeInfo);
    infoLayout->addWidget(CInfo);
    infoLayout->addWidget(OverlapInfo);
    
    QVBoxLayout *verticalLayout = new QVBoxLayout;
    verticalLayout->addWidget(scrollArea);
    verticalLayout->addLayout(pageNavigationLayout);
    verticalLayout->addLayout(parameterLayout);
    verticalLayout->addLayout(infoLayout);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(confirmButton);
    buttonsLayout->addWidget(closeButton);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(verticalLayout);

    mainLayout->addLayout(buttonsLayout);
    setLayout(mainLayout);
    resize(1000, 600);
   
    setWindowTitle(tr("Font Creater"));
    
    displayPage();
}


void PreprocessDialog::testFun()
{
	ncsaFindBar->setPreprocessParameters(resolutionSpinBox->value(), blockSizeSpinBox->value(), CSpinBox->value(), overlapSpinBox->value());
	displayPage();
}

void PreprocessDialog::goPrevious()
{
  currentPage = currentPage - 1;
  if(currentPage < 0)
  {
    currentPage = 0;
  }
  displayPage();
}

void PreprocessDialog::goNext()
{
  
  currentPage = currentPage + 1;
  if(currentPage >= doc->numPages())
  {
    currentPage = doc->numPages()-1;
  }
  displayPage();

}

void PreprocessDialog::displayPage()
{
	
  QImage originalPage = doc->page(currentPage)->renderToImage(resolutionSpinBox->value(), resolutionSpinBox->value()).convertToFormat(QImage::Format_RGB888);
  cv::Mat output = NCSAWordSpottingUtil::convertToBinary(originalPage, blockSizeSpinBox->value(), CSpinBox->value());
  QImage preview = NCSAWordSpottingUtil::cvmat2QImage(output);
  vector<int> boundaries = NCSAWordSpottingUtil::findLineBoundaries(output);
  vector<QRect> boxes = NCSAWordSpottingUtil::segmentImage(output, boundaries, overlapSpinBox->value());

  QPainter qPainter(&preview);
  qPainter.setBrush(Qt::NoBrush);
  qPainter.setPen(Qt::red);
  
  for(int i = 0; i < boxes.size(); i++) {
  	  qPainter.drawRect(boxes[i]);
  }
  
  qPainter.setBrush(Qt::NoBrush);
  qPainter.setPen(QPen(QColor(255, 255, 0, 100), 3, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));
  for(int i = 0; i < boundaries.size(); i++) {
  	  qPainter.drawLine(-5, boundaries[i], 1500, boundaries[i]);
  }


  preview.save("/home/htang14/intermediateresults/preview12345.png");
  imageLabel->setPixmap(QPixmap::fromImage(preview));
  imageLabel->adjustSize();
  QString pageText;
  pageText.append(QString::number(currentPage+1));
  pageText.append("/");
  pageText.append(QString::number(doc->numPages()));
  pageIndicator->setText(pageText);
  
}
#include "ncsaimagelabel.h"

// qt/kde includes
#include <qlabel.h>
#include <qmenu.h>
#include <qtoolbutton.h>
#include <qevent.h>
#include <kicon.h>
#include <klocale.h>
#include <kpushbutton.h>

// local includes
#include "searchlineedit.h"
#include "core/document.h"
#include <core/page.h>
#include "settings.h"

#include <QFontDatabase>
#include <qcombobox.h>
#include <QStringList>
#include <QPainter>
#include <QFileDialog>

#include <core/generator.h>
#include <tesseract/baseapi.h>
#include <tesseract/resultiterator.h>
#include <opencv2/opencv.hpp>

#include <kmessagebox.h>


void ImageLabel::mousePressEvent(QMouseEvent * e)
{
  QLabel::mousePressEvent(e);
  emit clicked(e->pos());
}

void ImageLabel::dragEnterEvent(QDragEnterEvent * e)
{
  QLabel::dragEnterEvent(e);
  qDebug() << "enter drag";
}
void ImageLabel::dragMoveEvent(QDragMoveEvent * e)
{
  QLabel::dragMoveEvent(e);
  qDebug() << "move drag";
}
void ImageLabel::dragLeaveEvent(QDragLeaveEvent * e)
{
  QLabel::dragLeaveEvent(e);
  qDebug() << "leave drag";
}
/***************************************************************************
 *   Copyright (C) 2013 by Haidong Tang                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef _NCSAIMAGELABEL_H_
#define _NCSAIMAGELABEL_H_

#include <QLabel>

class ImageLabel : public QLabel
{
    Q_OBJECT
protected:
  void mousePressEvent(QMouseEvent * e);
  void dragEnterEvent(QDragEnterEvent * e);
  void dragMoveEvent(QDragMoveEvent * e);
  void dragLeaveEvent(QDragLeaveEvent * e);
signals:
  void clicked(const QPoint & pos);
};



#endif
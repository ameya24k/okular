#include "ncsabuildfontdialog.h"


#include <qlabel.h>
#include <qmenu.h>
#include <qtoolbutton.h>
#include <qevent.h>
#include <kicon.h>
#include <klocale.h>
#include <kpushbutton.h>

// local includes
#include "searchlineedit.h"
#include "core/document.h"
#include <core/page.h>
#include "settings.h"

#include <QFontDatabase>
#include <qcombobox.h>
#include <QStringList>
#include <QPainter>
#include <QFileDialog>

#include <core/generator.h>
#include <tesseract/baseapi.h>
#include <tesseract/resultiterator.h>
#include <opencv2/opencv.hpp>

#include <kmessagebox.h>

BuildFontDialog::BuildFontDialog(NCSAFindBar *bar, Poppler::Document *pdf)
{

    ncsaFindBar = bar;
    doc = pdf;
    currentPage = 0;
    QPushButton *saveToFileButton = new QPushButton(tr("Save"));
    QPushButton *confirmButton = new QPushButton(tr("OK"));
    QPushButton *closeButton = new QPushButton(tr("Cancel"));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(confirmButton, SIGNAL(clicked()), this, SLOT(testFun()));
    connect(saveToFileButton, SIGNAL(clicked()), this, SLOT(saveToFile()));

    imageLabel = new ImageLabel;
    imageLabel->setBackgroundRole(QPalette::Window);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);
    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);
    connect(imageLabel, SIGNAL(clicked(const QPoint & )), this, SLOT(pickLetter(const QPoint & )));
    
    QPushButton *previousButton = new QPushButton(tr("Previous"));
    QPushButton *nextButton = new QPushButton(tr("Next"));
    connect(previousButton, SIGNAL(clicked()), this, SLOT(goPrevious()));
    connect(nextButton, SIGNAL(clicked()), this, SLOT(goNext()));
    pageIndicator = new QLabel;
    QRadioButton *displayOriginal = new QRadioButton(tr("Original"));
    QRadioButton *displayBoxed = new QRadioButton(tr("Boxed"));
    isBoxedDisplay.addButton(displayOriginal);
    isBoxedDisplay.addButton(displayBoxed);
    displayOriginal->setChecked(true);
    
    selectionArbitrary = new QCheckBox();
    selectionArbitrary->setChecked(false);
    selectionArbitrary->setText("Arbitrary Rectangle");
    arbitraryLastPoint.setX(-1);
    
    connect(displayOriginal, SIGNAL(clicked()), this, SLOT(displayOriginalPage()));
    connect(displayBoxed, SIGNAL(clicked()), this, SLOT(displayBoxedPage()));

    QHBoxLayout *pageNavigationLayout = new QHBoxLayout;
    pageNavigationLayout->addStretch();
    pageNavigationLayout->addWidget(previousButton);
    pageNavigationLayout->addWidget(pageIndicator);
    pageNavigationLayout->addWidget(nextButton);
    pageNavigationLayout->addStretch();
    pageNavigationLayout->addWidget(selectionArbitrary);
    pageNavigationLayout->addStretch();
    pageNavigationLayout->addWidget(displayOriginal);
    pageNavigationLayout->addWidget(displayBoxed);
    pageNavigationLayout->addStretch();
    
    
    QHBoxLayout *lowerCases = new QHBoxLayout;
    QHBoxLayout *upperCases = new QHBoxLayout;
    buttonGroup= new QButtonGroup;
    createLetters(lowerCases, upperCases);
    connect(buttonGroup,SIGNAL(buttonClicked(QAbstractButton*)),this,SLOT(displayFontEditorFor(QAbstractButton*)));

    QVBoxLayout *lettersLayout = new QVBoxLayout;
    lettersLayout->addLayout(lowerCases);
    lettersLayout->addLayout(upperCases);
    
    QHBoxLayout *letterAdjustAndLetters = new QHBoxLayout;
    fontEditor = new NCSAFontEditor(&builtFont);
    letterAdjustAndLetters->addLayout(fontEditor);
    letterAdjustAndLetters->addLayout(lettersLayout);

    QHBoxLayout *testLayout = new QHBoxLayout;
    testInput = new QLineEdit;
    testDisplay = new QLabel;
    testDisplay->setBackgroundRole(QPalette::WindowText);
    testLayout->addWidget(testInput);
    testLayout->addStretch();
    testLayout->addWidget(testDisplay);
    connect(testInput,SIGNAL(textChanged(QString)),this,SLOT(testInputChanged(QString)));
    
    QVBoxLayout *verticalLayout = new QVBoxLayout;
    verticalLayout->addWidget(scrollArea);
    verticalLayout->addLayout(pageNavigationLayout);
    verticalLayout->addLayout(letterAdjustAndLetters);
    verticalLayout->addLayout(testLayout);


    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(saveToFileButton);
    buttonsLayout->addWidget(confirmButton);
    buttonsLayout->addWidget(closeButton);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(verticalLayout);
    //mainLayout->addStretch(1);
    //mainLayout->addSpacing(12);
    mainLayout->addLayout(buttonsLayout);
    setLayout(mainLayout);
    resize(1000, 600);
   
    setWindowTitle(tr("Font Creater"));
    
    displayPage();
}



void BuildFontDialog::testFun()
{
  QLabel* label = radio2label[this->buttonGroup->checkedButton()];
  
  label->setText("X");
  
  QRadioButton *next= (QRadioButton *)(this->buttonGroup->button(this->buttonGroup->id(this->buttonGroup->checkedButton())-1));
  if(next != NULL)
  {
    next->setChecked(true);
  }
}

void BuildFontDialog::createLetters(QHBoxLayout *lowerCases, QHBoxLayout *upperCases)
{

      vector<char*> lowerLetterList;
      vector<char*> upperLetterList;

      lowerLetterList.push_back("a");
      lowerLetterList.push_back("b");
      lowerLetterList.push_back("c");
      lowerLetterList.push_back("d");
      lowerLetterList.push_back("e");
      lowerLetterList.push_back("f");
      lowerLetterList.push_back("g");
      lowerLetterList.push_back("h");
      lowerLetterList.push_back("i");
      lowerLetterList.push_back("j");
      lowerLetterList.push_back("k");
      lowerLetterList.push_back("l");
      lowerLetterList.push_back("m");
      lowerLetterList.push_back("n");
      lowerLetterList.push_back("o");
      lowerLetterList.push_back("p");
      lowerLetterList.push_back("q");
      lowerLetterList.push_back("r");
      lowerLetterList.push_back("s");
      lowerLetterList.push_back("t");
      lowerLetterList.push_back("u");
      lowerLetterList.push_back("v");
      lowerLetterList.push_back("w");
      lowerLetterList.push_back("x");
      lowerLetterList.push_back("y");
      lowerLetterList.push_back("z");
      upperLetterList.push_back("A");
      upperLetterList.push_back("B");
      upperLetterList.push_back("C");
      upperLetterList.push_back("D");
      upperLetterList.push_back("E");
      upperLetterList.push_back("F");
      upperLetterList.push_back("G");
      upperLetterList.push_back("H");
      upperLetterList.push_back("I");
      upperLetterList.push_back("J");
      upperLetterList.push_back("K");
      upperLetterList.push_back("L");
      upperLetterList.push_back("M");
      upperLetterList.push_back("N");
      upperLetterList.push_back("O");
      upperLetterList.push_back("P");
      upperLetterList.push_back("Q");
      upperLetterList.push_back("R");
      upperLetterList.push_back("S");
      upperLetterList.push_back("T");
      upperLetterList.push_back("U");
      upperLetterList.push_back("V");
      upperLetterList.push_back("W");
      upperLetterList.push_back("X");
      upperLetterList.push_back("Y");
      upperLetterList.push_back("Z");

      
      lowerCases->addStretch();
      for(int i = 0; i < lowerLetterList.size(); i++)
      {

	QVBoxLayout *layout = new QVBoxLayout();
	QLabel *letterImg = new QLabel();
	QRadioButton *a = new QRadioButton(lowerLetterList[i]);
	radioBtn2Char[a] = lowerLetterList[i][0];
	radio2label[a] = letterImg;
        layout->addWidget(letterImg);
	layout->addWidget(a);
	lowerCases->addLayout(layout);
	this->buttonGroup->addButton(a);
	if(i == 0) a->setChecked(true);
      }
      lowerCases->addStretch();
      
      upperCases->addStretch();
      for(int i = 0; i < upperLetterList.size(); i++)
      {
	QVBoxLayout *layout = new QVBoxLayout();
	QLabel *letterImg = new QLabel();
	QRadioButton *a = new QRadioButton(upperLetterList[i]);
	radioBtn2Char[a] = upperLetterList[i][0];
	radio2label[a] = letterImg;
        layout->addWidget(letterImg);
	layout->addWidget(a);
	upperCases->addLayout(layout);
	this->buttonGroup->addButton(a);
      }
      upperCases->addStretch();
  
}


void BuildFontDialog::goPrevious()
{
  currentPage = currentPage - 1;
  if(currentPage < 0)
  {
    currentPage = 0;
  }
  displayPage();
}

void BuildFontDialog::goNext()
{
  
  currentPage = currentPage + 1;
  if(currentPage >= doc->numPages())
  {
    currentPage = doc->numPages()-1;
  }
  displayPage();

}

void BuildFontDialog::displayOriginalPage()
{
	
  imageLabel->setPixmap(QPixmap::fromImage(originalPage));
  imageLabel->adjustSize();
}
void BuildFontDialog::displayBoxedPage()
{
  imageLabel->setPixmap(QPixmap::fromImage(boxedPage));
  imageLabel->adjustSize();
}
void BuildFontDialog::displayPage()
{
  if (ncsaFindBar->isHandwrittenMode()) {
  	 QImage coloredPage = doc->page(currentPage)->renderToImage(ncsaFindBar->NCSA_RESOLUTION, ncsaFindBar->NCSA_RESOLUTION).convertToFormat(QImage::Format_RGB888);
  	 cv::Mat output = NCSAWordSpottingUtil::convertToBinary(coloredPage, ncsaFindBar->NCSA_BLOCKSIZE, ncsaFindBar->NCSA_C);
    originalPage = NCSAWordSpottingUtil::cvmat2QImage(output).convertToFormat(QImage::Format_ARGB32);
    
    for (int x = 0; x < originalPage.width(); x++) {
      for (int y = 0; y < originalPage.height(); y++) {
      	QColor color(originalPage.pixel(x, y));
      	if (color.red() > 150) {
      		originalPage.setPixel(x, y, qRgba(0, 0, 0, 255));
      	} else {
      		originalPage.setPixel(x, y, qRgba(255, 255, 255, 0));
      	}
      	
      }    
    }
  } else {
    originalPage = doc->page(currentPage)->renderToImage(150, 150);
  }
  
  boxedPage = originalPage.copy();
  if (!ncsaFindBar->isHandwrittenMode()) {
  PIX* pix  = NCSAWordSpottingUtil::qImage2PIX(originalPage);
  tesseract::TessBaseAPI tess;
    tess.Init(NULL, "eng");
    
    tess.SetImage(pix);

    tess.Recognize(NULL);
    tesseract::ResultIterator* ri = tess.GetIterator();
    //tesseract::ChoiceIterator* ci;
    
    
    QPainter p;
    p.begin(&boxedPage);
    p.setPen(Qt::red);
    letterRects.clear();
    
    QRect lastLetterBoxTight(0,0,0,0);
    QRect lastWordBox(0,0,0,0);

    if(ri != NULL)
    {
      do
      {
	int left, top, right, bottom;
	ri->BoundingBox(tesseract::RIL_SYMBOL, &left, &top, &right, &bottom);
	int x1, y1, x2, y2;
	ri->Baseline(tesseract::RIL_SYMBOL, &x1, &y1, &x2, &y2);
	int baseline = y1 + ((left+right)/2-x1)*(y2-y1)/(x2-x1);
	baseline = baseline - top;
	QRect letterBoxTight = QRect(left, top, right-left, bottom-top);
	QRect letterBox = letterBoxTight;
	QRect wordBox = letterBoxTight;
	//tightLetterBox2Box[letterBoxTight] = letterBox;
	
	wordBox.setLeft(x1);
	wordBox.setRight(x2);
	letterRects.push_back(letterBoxTight);
	baselines.push_back(baseline);
	p.drawRect(letterBoxTight);
	
	/*
	if (lastLetterBoxTight.x() <= wordBox.x())
	{
	  QRect lastLetterBox = tightLetterBox2Box[lastLetterBoxTight];
	  int X1 = lastLetterBoxTight.x()+lastLetterBoxTight.width();
	  int X2 = letterBoxTight.x();
	  tightLetterBox2Box[lastLetterBoxTight] = QRect(lastLetterBox.x(),lastLetterBox.top(), (X1+X2)/2 - lastLetterBox.x(), lastLetterBox.height());
	  tightLetterBox2Box[letterBoxTight] = QRect((X1+X2)/2,letterBox.top(), letterBox.width(), letterBox.height());

	}*/
	
	lastLetterBoxTight = letterBoxTight;
	lastWordBox = wordBox;

      }
      while(ri->Next(tesseract::RIL_SYMBOL));
    }
    p.end();
      
    }
    
  imageLabel->setPixmap(QPixmap::fromImage(originalPage));
  imageLabel->adjustSize();
  QString pageText;
  pageText.append(QString::number(currentPage+1));
  pageText.append("/");
  pageText.append(QString::number(doc->numPages()));
  pageIndicator->setText(pageText);
  
  isBoxedDisplay.checkedButton()->click();


}

void BuildFontDialog::pickLetter(const QPoint & p)
{ 
  if (selectionArbitrary->isChecked())
  {
    if(arbitraryLastPoint.x() < 0)
    {
      arbitraryLastPoint = p;
    }
    else
    {
      QRect rect(arbitraryLastPoint, p);
      if(rect.height() <= 0) rect.setHeight(10);
      if(rect.width() <= 0) rect.setWidth(10); //TODO Code smell!!
      QImage picked = originalPage.copy(rect);
      QLabel* label = radio2label[this->buttonGroup->checkedButton()]; //TODO duplicate code!!!
      label->setPixmap(QPixmap::fromImage(picked));
      label->adjustSize();
      FontLetterInfo info;
      info.img = QPixmap::fromImage(picked);
      info.topLeft = QPoint(-2,-picked.height()) ;
      info.width = info.img.width()+4;
      //info.width = 0;
      char letter =  radioBtn2Char[(QRadioButton*)(this->buttonGroup->checkedButton())];
      builtFont[letter] = info;
      
      QRadioButton *next= (QRadioButton *)(this->buttonGroup->button(this->buttonGroup->id(this->buttonGroup->checkedButton())-1));
      if(next != NULL)
      {
        next->setChecked(true);
      }
      
      arbitraryLastPoint.setX(-1);
    }
    return;
  }
  for(int i = 0; i < letterRects.size(); i++)
  {
    QRect rect = letterRects[i];

    if(p.x() >= rect.left() && p.x() <= rect.right() && p.y() >= rect.top() && p.y() <= rect.bottom())
    {
      //qDebug() << "wrapped in" << p << rect;
      QImage picked = originalPage.copy(rect);
      QLabel* label = radio2label[this->buttonGroup->checkedButton()];
      label->setPixmap(QPixmap::fromImage(picked));
      label->adjustSize();
      
      
      
      int baseline = baselines[i]; //TODO : vulnerable!!!
      char letter =  radioBtn2Char[(QRadioButton*)(this->buttonGroup->checkedButton())];
      FontLetterInfo info;
      info.img = QPixmap::fromImage(picked);
      qDebug() << "baseline" << info.img.height() << baseline;
      info.topLeft = QPoint(-1,-baseline) ;
      info.width = info.img.width();
      builtFont[letter] = info;
      qDebug() << "add to map" << letter;
      
      
      QRadioButton *next= (QRadioButton *)(this->buttonGroup->button(this->buttonGroup->id(this->buttonGroup->checkedButton())-1));
      if(next != NULL)
      {
        next->setChecked(true);
      }
      
      break;
    }
  }
  
}

void BuildFontDialog::testInputChanged(QString str)
{
  QPixmap renderedText = renderFont2Pix(builtFont, str);
  testDisplay->setPixmap(renderedText);
  testDisplay->adjustSize();
}

QPixmap BuildFontDialog::renderFont2Pix(std::map<char, FontLetterInfo> builtFont, QString str)
{
  std::vector<FontLetterInfo> fontLetters;  
  for(int i = 0; i < str.size(); i++)
  {
    char letter = str.at(i).toAscii();
    if(builtFont.count(letter) != 0)
    {
      fontLetters.push_back(builtFont[letter]);
      qDebug() << letter;
    }
  }  
  
  int accumulatedWidth = 0;
  int leftMost = 0;
  int rightMost = 0;
  int topMost = 0;
  int bottomMost = 0;
  
  for(int i = 0; i < fontLetters.size(); i++)
  {
    if(accumulatedWidth + fontLetters[i].topLeft.x() < leftMost)
    {
      leftMost = accumulatedWidth + fontLetters[i].topLeft.x();
    }
    if( accumulatedWidth + fontLetters[i].topLeft.x() + fontLetters[i].img.width() > rightMost)
    {
      rightMost = accumulatedWidth + fontLetters[i].topLeft.x() + fontLetters[i].img.width();
    }
    if(fontLetters[i].topLeft.y() < topMost)
    {
      topMost = fontLetters[i].topLeft.y();
    }
    if(fontLetters[i].topLeft.y() + fontLetters[i].img.height() > bottomMost)
    {
      bottomMost = fontLetters[i].topLeft.y() + fontLetters[i].img.height();
    }
    accumulatedWidth += fontLetters[i].width;
  }
  
  qDebug() << leftMost << rightMost << topMost << bottomMost;
  QPixmap rendered(rightMost-leftMost, bottomMost - topMost);
  rendered.fill(Qt::transparent);
  QPainter p(&rendered);
  
  int baseline = 0 - topMost;
  int leftMargin = 0 - leftMost;
  
  accumulatedWidth = 0;
  for(int i = 0; i < fontLetters.size(); i++)
  {
    int x = accumulatedWidth + leftMargin + fontLetters[i].topLeft.x();
    qDebug() << "decomposed" << accumulatedWidth << leftMargin << fontLetters[i].topLeft.x();// x << y;
    int y = baseline + fontLetters[i].topLeft.y();
    qDebug() << x << y;
    p.drawPixmap(x, y, fontLetters[i].img);
    accumulatedWidth += fontLetters[i].width;
  }
  


  return rendered;
}


void BuildFontDialog::saveToFile()
{

  
  QString path = QFileDialog::getSaveFileName(this, "Save file", "", ".conf");
  QDir dir = QDir::root();
  if(!dir.mkdir(path))
  {
    qDebug() << "cannot create folder!";
  }
  else
  {

  	QFile file(path + "/fontInfo.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
   
 
    // optional, as QFile destructor will already do it:

  	for (std::map<char,FontLetterInfo>::iterator it = builtFont.begin(); it != builtFont.end(); ++it) {
  		char letter = it->first;
  		QPixmap img = it->second.img;
  		int width = it->second.width;
      QPoint topLeft = it->second.topLeft;
      img.save(path + '/' + letter + ".png");
      out << letter << "," << width << "," << topLeft.x() << "," << topLeft.y() << "\n";
  		}
  		    file.close(); 

  }
  
}

void BuildFontDialog::displayFontEditorFor(QAbstractButton*)
{
  char letter =  radioBtn2Char[(QRadioButton*)(this->buttonGroup->checkedButton())];
  fontEditor->displayEditorFor(letter);
}

/***************************************************************************
 *   Copyright (C) 2013 by Haidong Tang                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef _NCSAPREPROCESSDIALOG_H_
#define _NCSAPREPROCESSDIALOG_H_

#include <QDialog>
#include <core/document.h>
#include <poppler/qt4/poppler-qt4.h>

#include <QScrollArea>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include "ncsafindbar.h"
#include "ncsaimagelabel.h"
class NCSAFindBar;

class PreprocessDialog:public QDialog
{
  Q_OBJECT

public:
    PreprocessDialog(NCSAFindBar *bar, Poppler::Document *pdf);

public slots:
    void goPrevious();
    void goNext();
    void testFun();

private:
    QScrollArea * scrollArea;
    ImageLabel * imageLabel;
    QLabel * pageIndicator;

    Poppler::Document *doc;
    NCSAFindBar *ncsaFindBar;
    int currentPage;
    
    QSpinBox *resolutionSpinBox;
    QSpinBox *blockSizeSpinBox;
    QSpinBox *CSpinBox;
    QDoubleSpinBox *overlapSpinBox;
    
    void displayPage();
};


#endif
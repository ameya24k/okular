/***************************************************************************
 *   Copyright (C) 2007 by Pino Toscano <pino@kde.org>                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef _NCSAFINDBAR_H_
#define _NCSAFINDBAR_H_

#include <qwidget.h>
#include <QLabel>
#include <QComboBox>
#include <QDialog>
#include <qlayout.h>

#include <core/document.h>
#include <core/observer.h>

#include <leptonica/allheaders.h>

#include "ncsawordspottingutil.h"
#include "ncsafonteditor.h"
#include "ncsapreprocessdialog.h"
#include "ncsaimagelabel.h"
#include "ncsabuildfontdialog.h"
#include "ncsafontletterinfo.h"
#include <poppler/qt4/poppler-qt4.h>

class QAction;
class SearchLineWidget;

namespace Okular {
class Document;
}


#include <QListWidget>
#include <QListWidgetItem>
#include <QStackedWidget>
#include <QScrollArea>
#include <QButtonGroup>
#include <map>
#include <vector>
#include <QLabel>
#include <QRadioButton>
#include <QCheckBox>

class NCSAFontEditor;

class NCSAFindBar
    : public QWidget, private Okular::DocumentObserver
{
    Q_OBJECT

    public:
        explicit NCSAFindBar( Okular::Document * document, QWidget * parent = 0 );
        virtual ~NCSAFindBar();
        std::map<char, FontLetterInfo> builtFont;

        QString text() const;
        Qt::CaseSensitivity caseSensitivity() const;

        void focusAndSetCursor();
        bool maybeHide();
        
        void setPreprocessParameters(int res, int bs, int c, double ol);
        bool isHandwrittenMode();
        
	int NCSA_RESOLUTION;
   int NCSA_BLOCKSIZE;
   int NCSA_C;
   double NCSA_OVERLAP;
	
	enum {OKULAR_OBSERVER_ID = 6};
        uint observerId() const {
            return OKULAR_OBSERVER_ID;
        }
        void notifyPageChanged(int page, int flags);

    signals:
        void searchResultSelected(int pageNum, double x, double y, double w, double h);
        void searchResultSelectedShouldScroll(int pageNum, double y);

    //public slots:
        //void findNext();
        //void findPrev();
        //void resetSearch();

    private slots:
        //void caseSensitivityChanged();
        //void fromCurrentPageChanged();
        //void closeAndStopSearch();
	void changeFont(int index);
	void searchLineTextChanged(QString text);
	void performSearch();
	void bringPreprocessDialog();
	void resultComboBoxIndexChanged(int index);
	void resultComboBoxIndexSelected(int index);
	void buildFontAct(bool b);
	void fontSourceAsSystem(bool systemFont);
   void fontSourceAsCustom(bool customFont);
   void loadFont();

    private:
        Okular::Document * doc;
        //SearchLineWidget * m_search;
	QLineEdit *searchLine;
	QLabel * display;
	QComboBox *fontComboBox;
	QComboBox *resultComboBox;
        //QAction * m_caseSensitiveAct;
        //QAction * m_fromCurrentPageAct;
        //bool eventFilter( QObject *target, QEvent *event );
        bool m_active;
	void saveTempPics();
	void switchToSystemFont(bool flag);
	
	QString getFilePath();
	vector<NCSAWordInfo*> searchResult;
	NCSAWordSpottingUtil* wordSpottingUtil;
	
	void preprocessDocument();
	QAction *m_buildFontAct;
	QAction *m_useSystemFontAct;
	QAction *m_useCustomFontAct;
	QAction *m_handwrittenSegCB;
	QPushButton * loadFontBtn;
	std::map<char, FontLetterInfo> loadedFont;
	
	


	int lastProcessedResolution;
   int lastProcessedBlockSize;
   int lastProcessedC;
   double lastProcessedOverlap;
   bool lastProcessed;

	
};


#endif

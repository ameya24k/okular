INSTALLATION
Step 1: Make sure the following packages are installed by using command "sudo apt-get install package_name":

cmake
build-essential
kdelibs5-dev
libqimageblitz-dev
libpoppler-qt4-dev
libtesseract-dev
tesseract-ocr-eng
libleptonica-dev
libopencv-dev
git

Step 2: (This step is suggested on the okular's website)
Remember to adjust the environment variables PKG_CONFIG_PATH and LD_LIBRARY_PATH to make poppler reachable:

export PKG_CONFIG_PATH=<path to poppler install>/poppler/lib/pkgconfig:$PKG_CONFIG_PATH
export LD_LIBRARY_PATH=<path to poppler install>/poppler/lib:$LD_LIBRARY_PATH

Step 3: Run the following commands:
These will clone the code from our repository and build the executable. <target-foldername> should be the name of the folder that will contain the built Okular files.

git clone https://opensource.ncsa.illinois.edu/stash/scm/census/okular.git
cd okular
mkdir build
cd build
sudo cmake -DCMAKE_INSTALL_PREFIX=/home/<username>/<target-foldername> ..
sudo make
sudo make install
export KDEDIRS=/home/<username>/<target-foldername>:$(kde4-config --prefix); kbuildsycoca4

Step 4: Run Okular
Important: Notice that in order to have the NCSA additions enabled, a pdf must be loaded into Okular - Okular loads image files, but the NCSA additions are disabled in that case. 

cd /home/<username>/<target-foldername>/bin
./okular

